package main

import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

func main() {
	//initEvents()
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", homeLink)

	router.HandleFunc("/api/messages", createMessage).Methods("POST")
	router.HandleFunc("/api/messages/{id}", getOneUsersMessages).Methods("GET")
	router.HandleFunc("/api/messages/{id}", deleteMessage).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8080", router))
}