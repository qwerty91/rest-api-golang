package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"strconv"
	"github.com/gorilla/mux"
)

type message struct {
	ID          int    `json:"ID"`
	Title       string `json:"Title"`
	Content     string `json:"Content"`
	Email       string `json:"Email"`
}

type allMessages []message
var messages = allMessages{}
var uniqueCounter = 0
var messageMap = make(map[string]allMessages)

func homeLink(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome home!")
}

func createMessage(w http.ResponseWriter, r *http.Request) {
	var newMessage message
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Kindly enter data with the event title and description only in order to update")
	}
	
	json.Unmarshal(reqBody, &newMessage)
	newMessage.ID = uniqueCounter
	uniqueCounter ++
	
	messageMap[newMessage.Email] = append(messageMap[newMessage.Email], newMessage)
	log.Println("this is a content:", messageMap[newMessage.Email])
	
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newMessage)
}

func getOneUsersMessages(w http.ResponseWriter, r *http.Request) {
	messageID := mux.Vars(r)["id"]
	
	log.Println("wiadomosci uzytkownika %s:  %s", messageID, messageMap[messageID])
	
	json.NewEncoder(w).Encode(messageMap[messageID])
}

func deleteMessage(w http.ResponseWriter, r *http.Request) {
	messageID := mux.Vars(r)["id"]
	messageIdentifiers := strings.SplitAfter(messageID, "-")

	email := messageIdentifiers[0][:len(messageIdentifiers[0])-1]
	id := messageIdentifiers[1]

	log.Println("email: ", email)
	log.Println("id: ", id)

	idInt, err := strconv.Atoi(id)
    if err != nil {
        // handle error
        fmt.Println(err)
    }
	
	for i, singleMessage := range messageMap[email] {
		if singleMessage.ID == idInt {
			messageMap[email] = append(messageMap[email][:i], messageMap[email][i+1:]...)
			fmt.Fprintf(w, "The message with ID %v has been deleted successfully", messageID)
		}
	}
}